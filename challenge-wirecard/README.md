# Wirecard Configuration Management Challenge
[build-status](https://bitbucket.org/mannyci/exercise/addon/pipelines/home)

## Overview
Deployes webapp with Ansible on a Debian based container. The application runs behind a reverse proxy with SSL.

### Packages used
* Apache - Manages the reverse proxy with SSL
* Tomcat - Runs the Java applicaiton
* openJDK - JAVA runtime library

### How to
The `wrapper.sh` runs playbook on speific environment\hostgroup. The wrapper allows us to costomize few things for ansible playbook.

* Usage `./wrapper.sh <environment> <role> <extra_args>`
* Example: `./wrapper.sh dev all` - To run all roles on dev environment

## Ansible
Details of the components used in Ansible.

### Best practices
* All variables are defined in defaults
* The default variables should be overwritten following Ansible variable precedence
* Variable names must follow role name prefix
* Task names must follow, first letter capped
* Use handlers

### Roles
* apache - Installes and configures SSL and reverse proxy
* tomcat - Installes and configures tomcat on port 8080 and deployes the application as ROOT.war

### Tests
The `./wrapper.sh test` runs the unit testing for the roles. All tests are included in tasks/tests.yml and included if `ansible_unit_test is true`.
The tests runs all the tasks for each role and runs the tests after running the role tasks.

* To run tests for apache `./wrapper.sh test case-apache`
* To run tests for tomcat `./wrapper.sh test case-tomcat`
* `./wrapper.sh test all` for the complete workflow

## Outcome
### Result
Here is the expected outcome.
![Outcome](screenshot.png)

### Pipeline
Please check the latest pipeline status here(https://bitbucket.org/mannyci/exercise/addon/pipelines/home)