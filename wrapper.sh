#!/bin/bash
# Wrapper to run the ansible playbook
# With some customization gives us more options

dir=$(dirname $(readlink -f $0))
echo "Running Ansible play from :${dir}"
ENV=$1
export ANSIBLE_ROLES_PATH="${dir}/roles"
usage () {
    echo "Usage $0: hostgroup tag - run the playbook"
    echo "Usage $0: test case - test the playbook"
    exit 1
}

# Safe checks
if [ -z ${ENV} ]; then
    usage
fi

if [ ! -f ${dir}/playbooks/$ENV.yml ]; then
    usage
fi

ansible-playbook -i ${dir}/inventory/hosts ${dir}/playbooks/$ENV.yml --tags ${2} ${3} --private-key=/opt/atlassian/pipelines/agent/ssh/id_rsa
